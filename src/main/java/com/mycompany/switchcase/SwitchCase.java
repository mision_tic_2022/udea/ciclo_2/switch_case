
package com.mycompany.switchcase;

import java.util.Scanner;

public class SwitchCase {

    public static void main(String[] args) {
        
        int numero = 5;

        if(numero == 2){
            System.out.println("es igual a 2");
        }else if(numero == 5){
            System.out.println("es igual a 5");
        }

        switch(numero){
            case 1:
                System.out.println("es igual a 1");
                break;
            case 2: 
                System.out.println("es igual a 2");
                break;
            case 3:
                System.out.println("es igual a 3");
                break;
            case 4:
                System.out.println("es igual a 4");
                break;
            case 5:
                System.out.println("es igual a 5");
                break;
            default:
                System.out.println("No cumple ningún caso");
                break;
        }

        calculadora(10.5, 2.2, "*");
    }

    public static void calculadora(Double n1, Double n2, String operador){
        //Evaluar operador
        switch(operador){
            case "+":
                Double suma = n1+n2;
                System.out.println( "La suma es: "+suma );
                break;
            case "-":
                Double resta = n1-n2;
                System.out.println( "La resta es: "+resta );
                break;
            case "*":
                Double multiplicacion = n1*n2;
                System.out.println( "La multiplicación es: "+multiplicacion );
                break;
            case "/":
                Double division = n1/n2;
                System.out.println( "La división es: "+division );
                break;
            default:
                System.out.println( "No cumple ningún caso" );
                break;
        }
    }

    public static void solicitar_mes(){

        try(Scanner leer = new Scanner(System.in)){
            System.out.print("Ingrese el número del mes (1 a 6): ");
            int mes = leer.nextInt();
            //Evaluar el mes
            switch(mes){
                case 1:
                    System.out.println("Enero");
                    break;
                case 2:
                    System.out.println("Febrero");
                    break;
                case 3:
                    System.out.println("Marzo");
                    break;
                case 4:
                    System.out.println("Abril");
                    break;
                case 5: 
                    System.out.println("Mayo");
                    break;
                case 6:
                    System.out.println("Junio");
                    break;
                default:
                    System.out.println("No está dentro del rango solicitado");
                    break;
            }
        }catch(Exception error){
            System.out.println("Ingrese un número entero");
        }
    }

    //Desarrolle el ejercicio del "subsidio" utilizando switch-case
    public static void calcular_subsidio(int num_hijos){
        if(num_hijos > 0){
            switch(num_hijos){
                case 1:
                    System.out.println("Recibirá un subsidio de $500.000");
                    break;
                case 2:
                    System.out.println("Recibirá un subsidio de $1200.000");
                    break;
                case 3:
                    System.out.println("Recibirá un subsidio de $1800.000");
                    break;
                default:
                    System.out.println("Recibirá un subsidio de $3200.000");
                    break;
            }
        }else{
            System.out.println("No recibirá subsisio");
        }
        
    }

}
